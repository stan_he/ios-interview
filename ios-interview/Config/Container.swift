//
//  Container.swift
//  ios-interview
//
//  Created by Stan Feldman on 21/3/19.
//  Copyright © 2019 HealthEngine. All rights reserved.
//

import Swinject
import SwinjectAutoregistration

let container: Container = {
    let container = Container()
    container.registerDependencies()
    return container
}()

extension Container {
    
    func registerDependencies() {
        autoregister(DataProvider.self, initializer: DataProvider.init)
        autoregister(MainViewModel.self, initializer: MainViewModel.init)
    }
    
}
