//
//  Post.swift
//  ios-interview
//
//  Created by Stan on 20/03/2019.
//  Copyright © 2019 HealthEngine. All rights reserved.
//

import Mapper


class Post: Mappable {
	
	var text: String
	var authors: [User]
	
	required init(map: Mapper) throws {
		text = try map.from("text")
		authors = try map.from("authors")
	}
	
}
