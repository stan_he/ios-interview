//
//  User.swift
//  ios-interview
//
//  Created by Stan on 20/03/2019.
//  Copyright © 2019 HealthEngine. All rights reserved.
//

import Mapper


class User: Mappable {
	
	var name: String
	var interests: [String]
	
	required init(map: Mapper) throws {
		name = try map.from("name")
		interests = try map.from("interests")
	}
	
}
