//
//  DataProvider.swift
//  ios-interview
//
//  Created by Stan on 20/03/2019.
//  Copyright © 2019 HealthEngine. All rights reserved.
//

import RxSwift
import SwiftyJSON


class DataProvider {
	
	func getPosts() -> Single<[Post]> {
        do {
            let data = try Data(contentsOf: R.file.postsJson()!)
            if  let json = try JSON(data: data).arrayObject as NSArray?,
                let posts = Post.from(json) {
                return Single.just(posts).delaySubscription(RxTimeInterval(0.5), scheduler: MainScheduler.instance)
            } else {
                return Single.error(APIError.malformedData)
            }
        } catch _ {
            return Single.error(APIError.malformedData)
        }
	}
	
}

enum APIError: Error {

	case malformedData
	
}
