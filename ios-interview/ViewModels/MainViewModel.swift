//
//  MainViewModel.swift
//  ios-interview
//
//  Created by Stan Feldman on 21/3/19.
//  Copyright © 2019 HealthEngine. All rights reserved.
//

import RxSwift
import RxCocoa

class MainViewModel {
    
    var dataSource = BehaviorRelay<[Post]>(value: [])
    
    private var dataProvider: DataProvider = container.resolve(DataProvider.self)!
    private let disposeBag = DisposeBag()
    
    init() {
        dataProvider.getPosts().subscribe(onSuccess: { [weak self] posts in
            self?.dataSource.accept(posts)
        }, onError: { error in
            print(error)
        }).disposed(by: disposeBag)
    }
    
}
