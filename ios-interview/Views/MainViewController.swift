//
//  ViewController.swift
//  ios-interview
//
//  Created by Stan on 20/03/2019.
//  Copyright © 2019 HealthEngine. All rights reserved.
//

import UIKit
import RxSwift

class MainViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private let viewModel: MainViewModel = container.resolve(MainViewModel.self)!

	private let disposeBag = DisposeBag()
	
	override func viewDidLoad() {
		super.viewDidLoad()
        initUI()
        initBindings()
	}
    
    private func initUI() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(R.nib.postTableViewCell)
    }
    
    private func initBindings() {
        viewModel.dataSource.bind(onNext: reload).disposed(by: disposeBag)
    }

}

extension MainViewController: UITableViewDataSource, UITableViewDelegate {
    
    private func reload(posts: [Post]) {
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.dataSource.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.postTableViewCell, for: indexPath),
            indexPath.row < viewModel.dataSource.value.count {
            let post = viewModel.dataSource.value[indexPath.row]
            cell.titleLabel?.text = post.text
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
}
