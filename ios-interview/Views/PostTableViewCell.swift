//
//  PostTableViewCell.swift
//  ios-interview
//
//  Created by Stan Feldman on 21/3/19.
//  Copyright © 2019 HealthEngine. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    
}
